<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\cip_users;
use App\cip_pagos;
use App\cip_users_especialidad;
use Illuminate\Support\Facades\DB;

class getUsers extends Controller
{
    public function index(){
        $cip_user = cip_users::paginate(100);
        $hola = "hola comoe stas";

        return view('usuario.index',compact('cip_user','hola'));
    }
    public function create()
    {
        return view('usuario.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $message = new cip_users();

        $message -> name = $request->name;
        $message -> username = $request->username;
        $message -> email = $request->email;
        $message -> password = $request->password;
        
        $message -> usertype = $request->usertype;
        $message -> block = $request->block;
        $message -> registerDate = $request->registerDate;
        $message -> lastvisitDate = $request->lastvisitDate;
        $message -> activation = $request->activation;
        $message -> params = $request->params;
        $message -> codigoCIP = $request->codigoCIP;
        $message -> usuarioCreador = $request->usuarioCreador;

        $message -> fechaNacimiento = $request->fechaNacimiento;
        $message -> fechaModificacion = $request->fechaModificacion;
        


        $saved =$message -> save();

        $data = [];
        $data['success'] = $saved;
        return $data;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function searchName(Request $request)
    {
        $busqueda = $request->get('search');
        $cip_user = DB::table('cip_users')->where('name', 'like' , '%'. $busqueda.'%')->paginate(20);
        return view('usuario.index',compact('cip_user'));
    }
    public function searchDni(Request $request)
    {
        $busqueda = $request->get('search');
        $cip_user = DB::table('cip_users')->where('dni', 'like' , '%'. $busqueda.'%')->paginate(5);
        return view('usuario.index',compact('cip_user'));
    }
    public function searchCodigo(Request $request)
    {
        $busqueda = $request->get('search');
        $cip_user = DB::table('cip_users')->where('codigoCIP', 'like' , '%'. $busqueda.'%')->paginate(5);
        return view('usuario.index',compact('cip_user'));
    }
    public function show($id)
    {

        $busqueda = $id;
        $especialidad =  cip_users_especialidad::where('idUser', $busqueda)->get([
            'id',
            'idUser',
            'codigoCIP',
            'idEspecialidad',  
            'idInstitucion',  
            'fechaIncorporacion',  
            'fechaPromocion',  
            'fechaGraduacion',  
            'tituloProfesional',  
            'numeroResolucion',  
            'folioResolucion',  
            'hojaResolucion',  
            'fechaRevalidacion',  
            'resolucionRevalidacion',  
            'fechaInscripcion',  
            'fechaJuramentacion',  
            ]);
         //= cip_users_especialidad::find($id);
        $message = cip_users::find($id);
        //return $cip_user;
        return view('usuario.show',compact('message','especialidad'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {  
        $message = cip_users::find($id);
        return view('users.edit',compact('message'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $message = cip_users::find($id);

        $message -> paterno  = $request->paterno;
        $message -> materno  = $request->materno;
        $message -> name  = $request->name;
        $message -> username  = $request->username;
        $message -> email  = $request->email;
        $message -> celular  = $request->celular;
        $message -> dni  = $request->dni;
        $message -> codigoCIP  = $request->codigoCIP;
        $message -> usuarioCreador  = $request->usuarioCreador;
        $message -> direccion  = $request->direccion;
        

    
        if ($request->password) {
           $message -> password = bcrypt($request->password);
        }
     
        
        $saved =$message -> save();
        //return $message; 


        $data = [];
        $data['update'] = $saved;
        return $data;
    }
    public function EditarImagen (Request $request){
        //dd($request->all());
        $message = cip_users::find(7866);
         $file = $request->image;

         if ($file){
            $path = public_path('/img');
            $fileName = time().'.'.$file->getClientOriginalExtension();
            $moved = $file -> move($path, $fileName);
            $message -> nombreFoto = $fileName;
         }
         $saved =$message -> save();
         //return $message; 
 
 
         $data = [];
         $data['update'] = $saved;
         return $data;

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $message = cip_users::find($id);
        
        $message -> delete();
        
        
    }
    
}
