<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\getUsers;
Route::get('/set_language/{lang}', 'Controller@setLanguage')->name( 'set_language');

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('login', function () {
    return view ('seed.login');
});

Route::get('register', function () {
    return view('seed.register');
});
Route::get('forgotpassword', function () {
    return view('seed.forgotpassword');
});
Route::get('lock', function () {
    return view('index');
});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('/user', 'getUsers');
Route::get('/searchName', 'getUsers@searchName');
Route::get('/searchDni', 'getUsers@searchDni');
Route::get('/searchCodigo', 'getUsers@searchCodigo');

//imagen
Route::post('imagen','getUsers@EditarImagen');