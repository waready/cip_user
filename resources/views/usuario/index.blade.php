
@extends('seed.metronic')

@section('content')
    <div class="row">
        
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject bold uppercase">Cip-usuarios</span>
                    </div>
                    <div class="actions">
                        <div class="btn-group btn-group-devided" data-toggle="buttons">
                            <label class="btn btn-transparent dark btn-outline btn-circle btn-sm active">
                                <input type="radio" name="options" class="toggle" id="option1">Actions</label>
                            <label class="btn btn-transparent dark btn-outline btn-circle btn-sm">
                                <input type="radio" name="options" class="toggle" id="option2">Settings</label>
                        </div>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-toolbar">
                        <div class="row">
                            {{-- <div class="col-md-2">
                                <div class="btn-group">
                                    <button id="sample_editable_1_new" class="btn sbold green"> Agregar Nuevo 
                                        <i class="fa fa-plus"></i>
                                    </button>
                                </div>
                            </div> --}}
                            <div class="col-md-12">
                                <div class="form-inline">
                                    <form class="form-group" action="/searchName" method="get">
                                        <div class="form-group">
                                            <label>Nombres o Apellidos</label>
                                            <input type="search" name="search" class="form-control">
                                            <span class="form-group-btn">
                                                <button type="submit" class="btn bg-green-meadow bg-font-green-meadow">search</button>
                                            </span>
                                        </div>
                                    </form>
                                
                                
                                    <form class="form-group" action="/searchDni" method="get">
                                        <div class="form-group">
                                            <label> Busqueda dni</label>
                                            <input type="search" name="search" class="form-control">
                                            <span class="form-group-btn">
                                                <button type="submit" class="btn bg-green-meadow bg-font-green-meadow">search</button>
                                            </span>
                                        </div>
                                    </form>
                                
                                
                                    <form class="form-group" action="/searchCodigo" method="get">
                                        <div class="form-group">
                                            <label> Busqueda codigo Cip</label>
                                            <input type="search" name="search" class="form-control">
                                            <span class="form-group-btn">
                                                <button type="submit" class="btn bg-green-meadow bg-font-green-meadow">search</button>
                                            </span>
                                        </div>
                                    </form>
                                    <div class="btn-group">
                                        <a   href="user/create"class="btn sbold green"> Agregar Nuevo 
                                            <i class="fa fa-plus"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
                        <thead>
                            <tr>
                                <th>
                                    id
                                </th>
                                <th> NOMBRE </th>
                                <th> USERMANE </th>
                                <th> PHONE </th>
                                <th> EMAIL </th>
                                <th> CODIGO CIP </th>
                                <th> LUGAR </th>
                                <th> DNI </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($cip_user as $users )
                                <tr class="odd gradeX">
                                    <td>
                                        <a href="user/{{$users->id}}">
                                            <i class="fa fa-user"></i>
                                        </a>
                                    </td>
                                    <td>{{$users->name}}</td>
                                    <td>{{$users->username}}</td>
                                    <td>{{$users->celular}}</td>
                                    <td>{{$users->email}}</td>
                                    <td>{{$users->codigoCIP}}</td>
                                    <td>{{$users->direccion}}</td>
                                    <td>{{$users->dni}}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{ $cip_user->links() }}
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
@endsection
                    
             